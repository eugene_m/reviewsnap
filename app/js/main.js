$( document ).ready(function(){
   var trigger = $('#sideBarTrigger');
   var sideBar = $('#sideBar');
   var page = $('.pageWrapper');
   trigger.on('click', function(e){
       e.preventDefault();
       sideBar.toggleClass('in');
       page.toggleClass('min');
   })
});